class AddColumnsToLocation < ActiveRecord::Migration
  def change
    add_column :locations, :date, :string
    add_column :locations, :city, :string
    add_column :locations, :title, :string
    add_column :locations, :venue, :string
    add_column :locations, :region, :string
    add_column :locations, :postal_code, :string
    add_column :locations, :category, :string
    add_column :locations, :start_date, :string
    add_column :locations, :end_date, :string
    add_column :locations, :price, :string
  end
end
