require 'spec_helper'

FactoryGirl.define do 
  factory :location do
    name 'mike'
    address '4894 Whatever st.'
    id 1
  end
end