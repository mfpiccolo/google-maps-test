require 'spec_helper'

describe Event, :vcr do 

  context '#initialize' do 
    let(:vailid_parmeters) {{
      :latitude => '37.77493', 
      :longitude => '-122.419415',
      :api_key => 'O5EE3I72DNW2VXUOTH',
      :max => '3',
      :date => 'This week',
      :city => "Chicago",
      :within => '94117'
          }}
    let(:event) {Event.new(vailid_parmeters)}

    it 'initializes with an address' do
      event.should be_an_instance_of Event
    end

  end

  context 'readers' do
    it 'needs to be tested'
  end


end