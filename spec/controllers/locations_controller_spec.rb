require 'spec_helper'

describe LocationsController, :vcr do
  context 'routing' do
    it {should route(:get, '/locations/new').to :action => :new}
    it {should route(:post, '/locations').to :action => :create}
    it {should route(:get, '/locations/1/edit').to :action => :edit, :id => 1}
    it {should route(:put, '/locations/1').to :action => :update, :id => 1}
    it {should route(:delete, '/locations/1').to :action => :destroy, :id => 1}
    it {should route(:get, '/locations').to :action => :index}
    it {should route(:get, "/locations/1").to :action => :show, :id => 1}
  end

  context 'GET new' do
    before {get :new}

    it {should render_template :new}
  end

  context 'POST create' do
    context 'with valid parameters' do
      
      let(:valid_attributes) {{:address => '1234 Whever Dr., Fair Oaks, CA 95628', :id => 1}}
      let(:valid_parameters) {{:location => valid_attributes}}

      it 'creates a new location' do
        expect {post :create, valid_parameters}.to change(Location, :count).by(1)
      end

      before {post :create, valid_parameters}

      # it {should redirect_to location_path(location_id)}
      it {should set_the_flash[:notice]}
    end

    context 'with invalid parameters' 
    #   let(:invalid_attributes) {{: => ''}}
    #   let(:invalid_parameters) {{:contact => invalid_attributes}}
    #   before {post :create, invalid_parameters}

    #   it {should render_template :new}
    # end
  end

  context "GET #show" do
    let(:location) {FactoryGirl.create(:location)}
    before {get :show, id: location.id}

    it "assigns the requested contact to @contact" do
      get :show, id: location
      assigns(:location).should eq location
    end

    it {should render_template :show}
  end

  context 'GET edit' do
    let(:location) {FactoryGirl.create :location}
    before {get :edit, :id => location.id}

    it {should render_template :edit}
  end

  context 'PUT update' do
    let(:location) {FactoryGirl.create :location}

    context 'with valid parameters' do
      let(:valid_attributes) {{:address => '1234 Whever Dr., Fair Oaks, CA 95628'}}
      let(:valid_parameters) {{:id => location.id, :location => valid_attributes}}

      before {put :update, valid_parameters}

      it 'updates the location' do
        Location.find(location.id).address.should eq valid_attributes[:address]
      end

      it {should redirect_to Location.last}
      it {should set_the_flash[:notice]}
    end
  end

  context 'DELETE destroy' do
    it 'destroys a location' do
      location = FactoryGirl.create :location
      expect {delete :destroy, {:id => location.id}}.to change(Location, :count).by(-1)
    end

    let(:location) {FactoryGirl.create :location}
    before {delete :destroy, {:id => location.id}}

    it {should redirect_to locations_path}
    it {should set_the_flash[:notice]}
  end

  context 'GET index' do
    before {get :index}

    it {should render_template :index}
  end
end
