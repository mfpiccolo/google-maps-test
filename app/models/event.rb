class Event
  attr_reader :latitude, :longitude, :api_key, :max, :date, :city, :within, :events, :title, :venue, :address, :city, :region, :postal_code, :category, :start_date, :end_date, :price

  def initialize(options)

    @latitude = options[:latitude]
    @longitude = options[:longitude]
    @api_key = options[:api_key]
    @max = options[:max]
    @date = options[:date]
    @city = options[:city]
    @within = options[:within]
    @events = []

    urlencoded = URI.encode_www_form({'app_key' => @api_key, 'within' => '94117', 'latitude' => latitude, 'longitude' => longitude, 'max' => '3', 'date' => date})
    
    post_response = Faraday.get do |request|
      request.url "https://www.eventbrite.com/json/event_search?&#{urlencoded}"
    end
   
   
    @events = JSON.parse(post_response.body)['events']
   
    events.each do |event|
      if event.first.first == 'event'
        @title = event['event']['title']
        if event['event']['venue'] == nil
          @venue = ""
          @address = ""
          @city = ""
          @region = ""
          @postal_code = ""
          @latitude = ""
          @longitude = ""
        else
          @venue = event['event']['venue']['name']
          @address = event['event']['venue']['address']
          @city = event['event']['venue']['city']
          @region = event['event']['venue']['region']
          @postal_code = event['event']['venue']['postal_code']
          @latitude = event['event']['venue']['latitude']
          @longitude = event['event']['venue']['longitude']
        end
        @category = event['event']['category']
        @start_date = event['event']['start_date']
        @end_date = event['event']['end_date']
        @price = event['event']['tickets'].first['ticket']['price']
        Location.create(:latitude => @latitude, :longitude => @longitude, :date => @date, :title => @title, :venue => @venue, :address => @address, :city => @city, :region => @region, :postal_code => @postal_code, :category => @category, :start_date => @start_date, :end_date => @end_date, :price => @price)
      end
    end
  end
end