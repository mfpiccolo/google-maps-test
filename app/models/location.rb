class Location < ActiveRecord::Base
  attr_accessible :name, :gmaps, :latitude, :longitude, :api_key, :max, :date, :city, :within, :events, :title, :venue, :address, :city, :region, :postal_code, :category, :start_date, :end_date, :price
  acts_as_gmappable
 
  def gmaps4rails_address
    address
  end
end